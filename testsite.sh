#!/bin/bash

START=`date +%s`
INCR=`date +%s`

while [ $(( $(date +%s) - 3600 )) -lt $START ]; do
    
  if [ $(( $(date +%s) - 5 )) -gt $INCR ]; do
     echo "----"
     INCR=`date +%s`
  done
done