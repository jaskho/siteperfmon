#!/bin/bash


# + + + + +    CONFIGURATION SETTINGS   + + + + + + 

# URLS: bash array of urls to test. Provide full uris.
#URLS=(http://somesite.org http://somesite.org/about)
URLS=()

# FREQ: {integer} Frequency for test runs, in seconds.  Script will test $URLS every $FREQ seconds.
FREQ=300

# MAXRUNS: {integer} Max number of cycles to run. Use MAXRUNS=0 to run indefinitely. MAXRUNS is ignored if ENDTIME is specified.
MAXRUNS=0

# STARTTIME: {POSIX timestamp} Time at which to begin running tests.  Use STARTTIME=0 to begin testing immediately.
STARTTIME=0

# ENDTIME: {POSIX timestamp} Time at which to quit running tests.  Use ENDTIME=0 to begin testing immediately.
ENDTIME=0

# + + + + + + + + + + + + + + + + + + + + + + + + +


# todo: 
# + pass urls and other settings as command params
# + (??) interactive scripts
# + scheduled start/stop


while [[ "$1" != "" ]]; do
  case "$1" in
    -f | --frequency )          FREQ=$1
	                        ;;
    -m | --maxruns )            MAXRUNS=$1
	                        ;;
    -s | --start )              STARTTIME=$1
	                        ;;
    -e | --end )                ENDTIME=$1
	                        ;;
    -- )                        shift
				while [[ "$1" != "" ]]; do
                                  URLS+=("$1")
				  shift
			        done  
	                        ;;
    -e | --end )                ENDTIME=$1
	                        ;;
  esac
  shift
done

for s in "${URLS[@]}"; do echo ^^"$s"; done

exit 0


NOW=`date +%s`
LASTRUN="$NOW"
RUNCT=0

#while [ $(( $(date +%s) - 3600 )) -lt $NOW ]; do
# Loop until script is terminated by user

function runtests {
  local GOAHEAD=0
  local CANSTART=0
  local MUSTEND=0
  local RUNCTOK=0
  if (( STARTTIME == 0 || STARTTIME < NOW )); then CANSTART=1; fi
  if (( ENDTIME != 0 )) && (( ENDTIME < NOW )); then MUSTEND=1; fi
  #if ( (( ENDTIME -gt 0 )) )  ||  ( (( MAXRUNS == 0 )) || (( 1 == 1 )) ); then RUNCTOK=1; fi 
  if (( ENDTIME > 0 )) || (( RUNCT < MAXRUNS )); then RUNCTOK=1; fi 
  if (( CANSTART )) && ! (( MUSTEND )) && (( RUNCTOK )) ; then GOAHEAD=1; fi 
  dbg="$dbg
  NOW: $NOW
  MAXRUNS: $MAXRUNS
  RUNCT: $RUNCT
  STARTTIME: $STARTTIME
  ENDTIME: $ENDTIME
  ----
  CANSTART: $CANSTART
  MUSTEND: $MUSTEND
  RUNCTOK: $RUNCTOK
  GOAHEAD: $GOAHEAD
   ----
  TEST: $(( ENDTIME == 0 || ENDTIME > NOW ))"
#   TEST: $(( STARTTIME == 0 || STARTTIME > NOW ))
#   TEST: $((STARTTIME == 0))
#   TEST: $((STARTTIME < NOW))
#   TEST: $((STARTTIME == NOW))
#   TEST: $((STARTTIME > NOW))"

  return $GOAHEAD
}

# YES: start immediately, end time not reached
unset dbg
MAXRUNS=5
RUNCT=2
STARTTIME=0
ENDTIME=$((NOW + 1000))
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# YES: start earlier, end time not reached
unset dbg
MAXRUNS=5
RUNCT=2
STARTTIME=$((NOW - 1000))
ENDTIME=$((NOW + 1000))
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# YES: start earlier, no end time 
unset dbg
MAXRUNS=5
RUNCT=2
STARTTIME=$((NOW - 1000))
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# YES: start earlier, no end time, no max ct 
unset dbg
MAXRUNS=0
RUNCT=2
STARTTIME=$((NOW - 1000))
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# YES: start immediately, no end time, no max ct 
unset dbg
MAXRUNS=0
RUNCT=2
STARTTIME=0
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# YES: start immediately, no end time, max ct not exceeded
unset dbg
MAXRUNS=5
RUNCT=2
STARTTIME=0
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# NO: start time not reached
unset dbg
MAXRUNS=0
RUNCT=2
STARTTIME=$(( NOW + 1000 ))
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# NO: start time reached but end time exceeded
unset dbg
MAXRUNS=0
RUNCT=2
STARTTIME=$(( NOW - 1000 ))
ENDTIME=$(( NOW - 100 ))
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"


# NO: start time reached, no end time, but max runs exceeded
unset dbg
MAXRUNS=2
RUNCT=5
STARTTIME=$(( NOW - 1000 ))
ENDTIME=0
runtests
RUNTESTS=$?
echo "dbg: $dbg"
echo "------------------------"




if [[ runtests ]]; then echo "run tests says ok";else echo "runtests says NOT ok"; fi
echo "run tests exit: $?"
exit 0


      


while [ 1 == 1 ]; do    
  # Execute every FREQ seconds	
  d=$(date +%s)
  # if [ $(( $(date +%s) - $FREQ )) -gt $LASTRUN ]; then
  if [ $(( $d - $FREQ )) -gt $LASTRUN ]; then
    for url in "${URLS[@]}"; do
     echo "phantomas:  ${url}"
     f="logs/$(echo $url | sed 's/http\(s\|\):\/\///;s/\(:\|\/\)/../g')__${d}.txt"
     (phantomas --url "${url}" --verbose 2>> logs/tests.log 1> "$f")
     #encho "$d"asdf > $d
    done  
    LASTRUN=`date +%s`
  fi
  RUNCT=$((RUNCT + 1))
done

